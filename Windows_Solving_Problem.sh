

#A description of my troubleshooting process#
 Symptoms: When you try to connect to https://localhost/,the site fails to load, and you receive the following error message:503 
 this error occurs when the application pool that is associated with the web application does not start.                               
 First I checked the firewall & lowered the enhanced security protection next, I reviewed Eventviewer.
 Next, I checked httperr1.log file at c:\windows\system32\logfiles it showed Multiple 503 events.
 I had to review the DOCs of (IIS) Manager.

#What is wrong with it# 
 Eventid:5059 states "defaultapppool has been disabled (WAS) encountered a problem when it started a worker proccess to 
 serve the application pool"

#What I did to fix it#
 I first stopped & restarted all application pools. checked account permissions or IIS configuration.
 Adjusted the settings of the default application pool & classic classice.net In IIS then restarted.
 Verified http://localhost// connected without 503 error.

 FYI: In end I did not see the 503 error.

