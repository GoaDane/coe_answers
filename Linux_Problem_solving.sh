 #Exercise 3 Linux_Problem_solving#
  Redis
 
 #A short discription of your trouble shooting process#
 
  I started Redis, checked log for the errors. I used grep to see what was using port :6379.
 
 #Whats wrong with redis configuration#
 
    Increased maximum number of open files to 10032 (it was originally set to 1024).
 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add
  'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' 
   for this to take effect.
 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and
   memory usage issues with Redis. 
 # warning Creating Server TCP listening socket *:6379: bind: Address already in use
 
 #What I did to fix it#
  Solutions
   I sorted the logged errors, freed the Redis default port,
   added one line redis.conf - vm.overcommit_memory = 1 .
   I changed Tcp-back log from 500-1000.
   I made a change to two files 1:(/proc/sys/net/core/somaxconn) 128-->520, 2:(etc/rc.local) & 
   added the fix to the file for 2nd error:(echo never > /sys/kernel/mm/transparent_hugepage/enabled)
   Check all file, ulimit -a Setting “open files” Number, ulimit -n 10032
   
   #FYI: I really struggled with this however I learned alot. I sorted all the errors but I did not sort out# 
   #why I could not connect to redis-cli & with another day I am sure I would find a solution.# 
  