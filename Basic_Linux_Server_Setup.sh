#Exercise 2 Basic_Linux_Server_Setup#

#A bash script (batchScript) we can execute on an empty Linux(Ubuntu) machine#

#!/bin/bash 

# Curl installation
sudo apt install curl

# Docker installation

#removes docker related things if installed
sudo apt remove --yes docker docker-engine docker.io containerd runc

#Updates system
sudo apt update

#install some useful dependencies
sudo apt --yes --no-install-recommends install apt-transport-https ca-certificates

#get from an URL and add a key
wget --quiet --output-document=- https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#add a repo
sudo add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu $(lsb_release --codename --short) stable"

sudo apt update

#install docker dependencies
sudo apt --yes --no-install-recommends install docker-ce docker-ce-cli containerd.io

#adds current user in docker group
sudo usermod --append --groups docker "$USER"

#make docker enabled on system
sudo systemctl enable docker

#runs elasticsearch in a docker container, takes elasticsearch's port 9200 output and redirect it to port 9200 of container, same
#for 9300.
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.8.13

#join the container, that's on port 9200 of localhost, in /_cat/health directory
curl localhost:9200/_cat/health


#FYI: I had several issues with this but with alot of stack over flow it worked out.#